@extends('layouts.main')
@section('name', 'users')
@section('content')
    <div class="row my-1">
        <div class="col-md-12">
            <a class="btn btn-primary float-end" href="{{ route('u.index') }}">Create new user</a>
        </div>
    </div>
    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
       @foreach($users as $user)
           <tr>
               <th scope="row">{{$user->id}}</th>
               <td>{{$user->name}}</td>
               <td>{{$user->email}}</td>
               <td>
                   <a class="btn btn-warning">SHOW</a>
                   <a class="btn btn-secondary">EDIT</a>
                   <a class="btn btn-danger">DELETE</a>
               </td>
           </tr>
           @endforeach
        </tbody>
    </table>
@endsection
